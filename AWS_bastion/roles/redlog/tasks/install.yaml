---
- name: Prepare logstash files & redis files
  block:

  - name: Check for java exists in /tmp   
    stat: path=/tmp/java.zip
    register: folder

  - name: Upload Java software to the instance
    shell: cd /tmp/ && scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i my_aws.pem oracle180.rpm ec2-user@{{ logstash_privateip_log }}:/tmp/oracle180.rpm

  - name: Check for redis exists in /tmp   
    stat: path=/tmp/redis.zip
    register: folder

  - name: Download Redis from AWS to bastion
    command: sudo yum install redis-3.2.12-2.el6.x86_64 --downloadonly --downloaddir=/tmp/redis
    when: not folder.stat.exists

  - name: Zip redis installation files
    shell: cd /tmp/ && sudo zip -r redis.zip redis/
    when: not folder.stat.exists

  - name: Send Redis to Redis Logstash server
    command: sudo scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i /tmp/my_aws.pem /tmp/redis.zip ec2-user@{{logstash_privateip_log}}:/tmp/redis.zip

  - name: Check for logstash exists in /tmp   
    stat: path=/tmp/logstash-7.6.1.rpm
    register: folder

  - name: Download Logstash from AWS to bastion
    shell: cd /tmp/ && curl -L -O https://artifacts.elastic.co/downloads/logstash/logstash-7.6.1.rpm
    when: not folder.stat.exists
    
  - name: Send Logstash to Redis Logstash server
    command: sudo scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i /tmp/my_aws.pem /tmp/logstash-7.6.1.rpm ec2-user@{{logstash_privateip_log}}:/tmp/logstash-7.6.1.rpm

#- name: Configure certificates
#  block:
#  - name: Check for stack cert exists  
#    stat: path=/etc/ssl/private/stack.pem
#    register: folder

#  - name: Generate an OpenSSL private key with the default values (4096 bits, RSA)
#    openssl_privatekey:
#      path: /etc/ssl/private/stack.pem
#      cipher: aes256
#      passphrase: stack
#      type: DSA
#    when: not folder.stat.exists

#  - name: Generate an OpenSSL private key with the default values (4096 bits, RSA)
#    openssl_privatekey:
#      path: /etc/ssl/private/logstash.pem
#      cipher: aes256
#      type: DSA
#
#  - name: Generate an OpenSSL Certificate Signing Request
#    openssl_csr:
#      path: /etc/ssl/csr/logstash.csr
#      privatekey_path: /etc/ssl/private/logstash.pem
#      common_name: tfmStack
#      #key_usage: Signature
#
#  - name: Generate certificate
#    openssl_certificate:
#      path: /etc/ssl/crt/logstash.crt
#      privatekey_path: /etc/ssl/private/stack.pem
#      csr_path: /etc/ssl/csr/logstash.csr
#      provider: selfsigned
#
#  - name: Send Logstash.pem
#    command: sudo scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i /tmp/my_aws.pem /etc/ssl/private/logstash.pem ec2-user@{{logstash_privateip_log}}:/etc/ssl/private/logstash.pem
#
#  - name: Send Stack.pem
#    command: sudo scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i /tmp/my_aws.pem /etc/ssl/private/stack.pem ec2-user@{{logstash_privateip_log}}:/etc/ssl/private/stack.pem
#
#  - name: Send Logstash.crt
#    command: sudo scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -i /tmp/my_aws.pem /etc/ssl/crt/logstash.crt ec2-user@{{logstash_privateip_log}}:/etc/ssl/crt/logstash.crt

- name: Install Java 1.8 
  block:

  - name: Install java 1.8 orace
    command: rpm -ivh /tmp/oracle180.rpm
    ignore_errors: true

  - name: Change Java version from 1.7 to 1.8
    alternatives:
      name: java
      link: /usr/bin/java
      path: /usr/java/jdk1.8.0_251-amd64/jre/bin/java

  delegate_to: "{{logstash_privateip_log}}"

- name: Install redis 
  block:

  - name: Check for redis exists in /tmp   
    stat: path=/tmp/redis
    register: folder

  - name: Unzip files 
    shell: cd /tmp/ && sudo unzip -q redis.zip 
    when: not folder.stat.exists

  - name: Check if jemalloc-3.3.1-1.8.amzn1.x86_64.rpm is installed
    command: rpm -q jemalloc-3.3.1-1.8.amzn1.x86_64
    ignore_errors: true
    register: rpm_check

  - name: Install redis dependency
    command: rpm -U /tmp/redis/jemalloc-3.3.1-1.8.amzn1.x86_64.rpm
    when: rpm_check.stdout.find('is not installed') != -1

  - name: Check if redis-3.2.12-2.el6.x86_64 is installed
    command: rpm -q redis-3.2.12-2.el6.x86_64
    ignore_errors: true
    register: rpm_check

  - name: Install redis
    command: rpm -U /tmp/redis/redis-3.2.12-2.el6.x86_64.rpm
    when: rpm_check.stdout.find('is not installed') != -1

  - name: Replace the binded host in redis conf
    lineinfile:
      path: /etc/redis.conf
      regexp: 'bind 127.0.0.1'
      line: "bind {{logstash_privateip_log}}"
      owner: root
      group: root
      mode: '644'
    when: rpm_check.stdout.find('is not installed') != -1
  
  - name: Start service Redis, if not started
    service:
      name: redis
      state: started

  delegate_to: "{{logstash_privateip_log}}"


- name: Install logstash server
  block:

  - name: Check if logstash-7.6.1.rpm is installed
    command: rpm -q logstash-7.6.1-1.noarch
    ignore_errors: true
    register: rpm_check

  - name: Install amazon plugin
    command: rpm -U /tmp/logstash-7.6.1.rpm
    when: rpm_check.stdout.find('is not installed') != -1

  - name: Replace the Xms y Xmg jvm.options logstash
    lineinfile:
      path: /etc/logstash/jvm.options
      regexp: '-Xms1g'
      line: "-Xms512m"
      owner: root
      group: root
      mode: '644'
    when: rpm_check.stdout.find('is not installed') != -1

  - name: Replace the Xms y Xmg jvm.options logstash
    lineinfile:
      path: /etc/logstash/jvm.options
      regexp: '-Xmx1g'
      line: "-Xmx512m"
      owner: root
      group: root
      mode: '644'
    when: rpm_check.stdout.find('is not installed') != -1

  - name: Register Logstash in service
    command:  /usr/share/logstash/bin/system-install /etc/logstash/startup.options sysv
    when: rpm_check.stdout.find('is not installed') != -1
    
  - name: Configure logstash server
    block:
    
    - name: Check if my-pipeline.conf file exists.
      stat:
        path: /etc/logstash/conf.d/my-pipeline.conf
      register: file_details

    - name: Create my-pipeline.conf
      file:
        path: /etc/logstash/conf.d/my-pipeline.conf
        state: touch
        mode: '644'
      when: file_details.stat.exists == False 

    - name: Insert Input logs for logstash
      blockinfile:
        path: /etc/logstash/conf.d/my-pipeline.conf
        block: |
          input {
            redis {
              host => "{{logstash_privateip_log}}"
              port => "6379"
              data_type => "list"
              key => "filebeat"
            }
          }
          filter {
              grok {
                  match => { "message" => "%{COMBINEDAPACHELOG}"}
              }
              geoip {
                  source => "clientip"
              }
              date {
                match => [ "timestamp" , "dd/MMM/yyyy:HH:mm:ss Z" ]
              }
          }
          output {
            elasticsearch {
              id => "logstash"
              index => "filebeat-"
              hosts => ["{{elastic_privateip_elas}}:9200"]
            }
            stdout { codec => rubydebug }
          }


      when: file_details.stat.exists == False 
  
  - name: Install systemctl filebeat service
    command: sudo /usr/share/logstash/bin/system-install /etc/logstash/startup.options systemd
    ignore_errors: true
      

  - name: Start service Logstash, if not started
    service:
      name: logstash
      state: started

  delegate_to: "{{logstash_privateip_log}}"